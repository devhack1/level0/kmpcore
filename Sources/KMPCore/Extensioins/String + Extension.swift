//
//  File.swift
//  
//
//  Created by Александр Лавринович on 05.06.2021.
//

import UIKit

public extension String {

    var attributed: NSMutableAttributedString {
        return NSMutableAttributedString(string: self)
    }
    
    var range: NSRange {
        return NSRange(location: 0, length: self.utf16.count)
    }
    
    func calculateSizeFast(font: UIFont, width: CGFloat = .greatestFiniteMagnitude) -> CGSize {
        let attributed = self
            .attributed
            .font(font)
            .lineSpacingMultiplier(1.16)
        
        return attributed.boundingRect(with: CGSize(width: width,
                                                    height: .greatestFiniteMagnitude),
                                       options: .usesLineFragmentOrigin,
                                       context: nil).size
    }
    
    func calculateSize(font: UIFont, width: CGFloat = .greatestFiniteMagnitude, spaced: Bool = false, maxLines: Int = 0) -> CGSize {
        let label = UILabel()
        label.numberOfLines = maxLines
        
        var attributedText = self
            .attributed
            .font(font)
        
        if spaced {
            attributedText = attributedText
                .lineSpacingMultiplier(1.16)
        }
        
        label.attributedText = attributedText

        return label.systemLayoutSizeFitting(CGSize(width: width,
                                                    height: UIView.layoutFittingCompressedSize.height),
                                             withHorizontalFittingPriority: .required,
                                             verticalFittingPriority: .fittingSizeLevel)
    }
    
    func calculateSize(of attributedText: NSAttributedString, width: CGFloat = .greatestFiniteMagnitude, maxLines: Int = 0) -> CGSize {
        let label = UILabel()
        label.numberOfLines = maxLines
        
        label.attributedText = attributedText

        return label.systemLayoutSizeFitting(CGSize(width: width,
                                                    height: UIView.layoutFittingCompressedSize.height),
                                             withHorizontalFittingPriority: .required,
                                             verticalFittingPriority: .fittingSizeLevel)
    }

    func testCalculateSize(font: UIFont, width: CGFloat = .greatestFiniteMagnitude) -> CGSize {
        let label = UILabel()
        label.numberOfLines = 2
        label.attributedText = self
            .attributed
            .font(font)

        return label.systemLayoutSizeFitting(CGSize(width: width,
                                                    height: UIView.layoutFittingCompressedSize.height),
                                             withHorizontalFittingPriority: .required,
                                             verticalFittingPriority: .fittingSizeLevel)
    }
}
