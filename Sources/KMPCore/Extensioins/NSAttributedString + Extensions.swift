//
//  File.swift
//  
//
//  Created by Александр Лавринович on 05.06.2021.
//

import UIKit

public typealias StringAttributes = [NSAttributedString.Key: Any]

public extension NSAttributedString {
    func lineHeightMultiple() -> CGFloat {
        if self.length == 0 { return 1.0 }
        let attributes = self.attributes(at: 0, effectiveRange: nil)
        if let value = attributes[.paragraphStyle] as? NSParagraphStyle {
            return value.lineHeightMultiple
        }
        return 1.0
    }
}

public extension Dictionary where Key == NSAttributedString.Key, Value == Any {
    
    func color(_ color: UIColor) -> [Key: Value] {
        var new = self
        
        new[.foregroundColor] = color
        
        return new
    }
    
    func underline(_ color: UIColor) -> [Key: Value] {
        var new = self
        
        new[.underlineColor] = color
        
        return new
    }

    func strikeThrough(style: NSUnderlineStyle, color: UIColor) -> [Key: Value] {
        var new = self

        new[.strikethroughStyle] = style.rawValue
        new[.strikethroughColor] = color

        return new
    }
    
    func font(_ font: UIFont?) -> [Key: Value] {
        var new = self
        
        new[.font] = font
        
        return new
    }
    
    func alignment(_ value: NSTextAlignment?, style: NSMutableParagraphStyle? = nil) -> [Key: Value] {
        var new = self
        
        guard let value = value else { return new }
        
        let paragraph: NSMutableParagraphStyle
        
        if let parameterStyle = style {
            paragraph = parameterStyle
        } else if let storedStyle = new[.paragraphStyle] as? NSMutableParagraphStyle {
            paragraph = storedStyle
        } else {
            paragraph = NSMutableParagraphStyle()
        }
        
        paragraph.alignment = value
        new[.paragraphStyle] = paragraph
        
        return new
    }
    
    func lineSpacing(_ value: CGFloat, style: NSMutableParagraphStyle? = nil) -> [Key: Value] {
        var new = self
        
        let paragraph: NSMutableParagraphStyle
        
        if let parameterStyle = style {
            paragraph = parameterStyle
        } else if let storedStyle = new[.paragraphStyle] as? NSMutableParagraphStyle {
            paragraph = storedStyle
        } else {
            paragraph = NSMutableParagraphStyle()
        }
        
        paragraph.lineSpacing = value
        new[.paragraphStyle] = paragraph
        
        return new
    }
    
    func lineSpacingMultiplier(_ value: CGFloat, style: NSMutableParagraphStyle? = nil) -> [Key: Value] {
        var new = self
        
        let paragraph: NSMutableParagraphStyle
        
        if let parameterStyle = style {
            paragraph = parameterStyle
        } else if let storedStyle = new[.paragraphStyle] as? NSMutableParagraphStyle {
            paragraph = storedStyle
        } else {
            paragraph = NSMutableParagraphStyle()
        }
        
        paragraph.lineHeightMultiple = value
        new[.paragraphStyle] = paragraph
        
        return new
    }
    
    func letterSpacing(_ value: CGFloat) -> [Key: Value] {
        var new = self
        
        new[.kern] = value
        
        return new
    }
    
    func truncateMode(_ value: NSLineBreakMode, style: NSMutableParagraphStyle? = nil) -> [Key: Value] {
        var new = self
        
        let paragraph: NSMutableParagraphStyle
        
        if let parameterStyle = style {
            paragraph = parameterStyle
        } else if let storedStyle = new[.paragraphStyle] as? NSMutableParagraphStyle {
            paragraph = storedStyle
        } else {
            paragraph = NSMutableParagraphStyle()
        }
        
        paragraph.lineBreakMode = value
        new[.paragraphStyle] = paragraph
        
        return new
    }
}

public extension NSMutableAttributedString {
    
    func attributeRange(using optionalRange: NSRange?) -> NSRange {
        let range: NSRange
        
        if let requiredRange = optionalRange {
            range = requiredRange
        } else {
            range = self.string.range
        }
        
        return range
    }
    
    func style(at range: NSRange) -> NSMutableParagraphStyle? {
        guard range.length > 0 else { return nil }
        let currentStyle = self.attribute(.paragraphStyle, at: 0, longestEffectiveRange: nil, in: range) as? NSParagraphStyle
        let style = currentStyle?.mutableCopy() as? NSMutableParagraphStyle

        return style
    }
    
    @discardableResult
    func color(at optionalRange: NSRange? = nil, _ color: UIColor) -> Self {
        self.addAttributes(StringAttributes().color(color), range: self.attributeRange(using: optionalRange))
        
        return self
    }
    
    @discardableResult
    func underline(at optionalRange: NSRange? = nil, _ color: UIColor) -> Self {
        self.addAttributes(StringAttributes().underline(color), range: self.attributeRange(using: optionalRange))
        
        return self
    }

    @discardableResult
    func strikeThrough(style: NSUnderlineStyle, color: UIColor, at range: NSRange? = nil) -> Self {
        self.addAttributes(StringAttributes().strikeThrough(style: style, color: color),
                           range: self.attributeRange(using: range))

        return self
    }
    
    @discardableResult
    func font(at optionalRange: NSRange? = nil, _ font: UIFont) -> Self {
        self.addAttributes(StringAttributes().font(font), range: self.attributeRange(using: optionalRange))
        
        return self
    }
    
    @discardableResult
    func truncate(at optionalRange: NSRange? = nil, _ truncatingMode: NSLineBreakMode) -> Self {
        let range = self.attributeRange(using: optionalRange)
        let currentStyle = self.style(at: range)
        self.addAttributes(StringAttributes().truncateMode(truncatingMode, style: currentStyle), range: range)
        
        return self
    }
    
    @discardableResult
    func alignment(at optionalRange: NSRange? = nil, _ value: NSTextAlignment) -> Self {
        let range = self.attributeRange(using: optionalRange)
        let currentStyle = self.style(at: range)
        self.addAttributes(StringAttributes().alignment(value, style: currentStyle), range: range)
        
        return self
    }
    
    @discardableResult
    func lineSpacingMultiplier(at optionalRange: NSRange? = nil, _ value: CGFloat) -> Self {
        let range = self.attributeRange(using: optionalRange)
        let currentStyle = self.style(at: range)
        self.addAttributes(StringAttributes().lineSpacingMultiplier(value, style: currentStyle), range: range)
        
        return self
    }
    
    @discardableResult
    func lineSpacing(at optionalRange: NSRange? = nil, _ value: CGFloat, style: NSMutableParagraphStyle? = nil) -> Self {
        let range = self.attributeRange(using: optionalRange)
        guard let newStyle = style else {
            let currentStyle = self.style(at: range)
            self.addAttributes(StringAttributes().lineSpacing(value, style: currentStyle), range: range)
            return self
        }
        self.addAttributes(StringAttributes().lineSpacing(value, style: newStyle), range: range)
        
        return self
    }
    
    @discardableResult
    func letterSpacing(at optionalRange: NSRange? = nil, _ value: CGFloat) -> Self {
        self.addAttributes(StringAttributes().letterSpacing(value), range: self.attributeRange(using: optionalRange))
        
        return self
    }
    
    @discardableResult
    func hyphenate(at optionalRange: NSRange? = nil, _ truncatingMode: NSLineBreakMode) -> Self {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.hyphenationFactor = 1.0
        let range = self.attributeRange(using: optionalRange)
        self.addAttributes(StringAttributes().truncateMode(truncatingMode, style: paragraphStyle), range: range)
        
        return self
    }
}
