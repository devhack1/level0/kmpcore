//
//  File.swift
//  
//
//  Created by Вильян Яумбаев on 06.06.2021.
//

import UIKit

public protocol DeeplinkManagerProtocol {
    func canResolve(url: URL) -> Bool
    func resolve(url: URL)
}

public protocol DeeplinkCoordinatorProtocol: AnyObject {
    func present(controller: UIViewController)
}

public protocol DeeplinkCoordinatorContainable: AnyObject {
    var deeplinkCoordinator: DeeplinkCoordinatorProtocol? { get }
}
