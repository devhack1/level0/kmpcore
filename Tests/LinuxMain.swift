import XCTest

import KMPCoreTests

var tests = [XCTestCaseEntry]()
tests += KMPCoreTests.allTests()
XCTMain(tests)
